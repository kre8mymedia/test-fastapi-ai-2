# FastAPI Server

This repository contains the code for a FastAPI server that exposes a `/ping` endpoint and a `/project` endpoint.

## Requirements

- Python 3.9 or later
- pip

## Installation

1. Clone this repository
2. Navigate to the project directory: `cd fastapiserver`
3. Create a virtual environment: `python3 -m venv env`
4. Activate the virtual environment: `source env/bin/activate`
5. Install the dependencies: `pip install -r requirements.txt`

## Usage

To start the development server, run:

```bash
uvicorn main:app --reload --port 8000
```

The server will be accessible at `http://localhost:8000`.

## Testing

To run the tests, run:

```bash
python3 -m pytest test.py
```

## Deployment

To build and deploy the Docker image, run:

```bash
docker-compose build
docker-compose up
```

## License

This project is licensed under the MIT License. See [LICENSE](LICENSE) for more details.
